/*�������� ���������, ������� ��������� �������� ���� �� �����-
��� � �������, �, ��������, � ����������� �� ������� ��� �����.
��������: 45.00D - �������� �������� � ��������, � 45.00R - �
��������. ���� ������ �������������� �� ������� %f%c
*/

// �������� ������� �.�.

#include <stdio.h>
#include <string.h>  // ��������� ������ �� ��������

#define pi 3.14 

void clean_stdin()  // ������� ������� ������
{
     int c;
     do
     {
         c=getchar();
     }
     while (c!='\n' && c!=EOF);
}


int main ()
{
    float degree=0;     //input value
    float outdegree;    //output value 
    char system=0;      //radians or degrees
    char cont=0;        // y/n

    while (1)
    {
        puts("Enter your value: 45.00D/R");
        if (scanf("%f%c", &degree, &system)<2 ||
            (system !='D' & system !='R'))
        {
            puts ("Input Error!");
            clean_stdin ();
        }
        else
        {
            if (system == 'D')
            {
                outdegree=degree/180;
                printf ("%fPi radians \n", outdegree);
            }
            if (system == 'R')
            {
                outdegree=degree/pi*180;
                printf ("%f degrees \n", outdegree);
            }

            clean_stdin ();
            puts ("One more time? y?");
            scanf ("%c", &cont);
            if (cont != 'y') 
            {
                clean_stdin ();
                break;
            }

        }
    }
return 0;
}
    