/*
�������� ���������, ������� ����������� ������� ����� � ���-
���� ��:��:��, � ����� ������� ����������� � ����������� ��
���������� ������� ("������ ���� "������ ����"� �.�.)
*/

// �������� ������� �.�.

#include <stdio.h>
#include <string.h>  // ��������� ������ �� ��������
#define Max_Morning 10       
#define Max_Day 16
#define Max_Evening 22           
#define Max_Night 4

void clean_stdin()  // ������� ������� ������
{
     int c;
     do
     {
         c=getchar();
     }
     while (c!='\n' && c!=EOF);
}

//F10, shift+F5 - ����/����� �� �������

int main ()
{
    int hours;
    int minutes;
    int seconds;

    while (1)
    {
        puts ("How much time? hh:mm:ss");
        if (scanf ("%d:%d:%d", &hours, &minutes, &seconds)<3 ||
            hours>=24 || hours<0 ||
            minutes>60 || minutes<0 ||
            seconds>60 || seconds<0)
        {
            puts ("Input Error");
            clean_stdin();
        }
        else
        {
            if (hours>=Max_Morning && hours<Max_Day)
            {
                puts ("Good day!");
                break;
            }
            if (hours>=Max_Day && hours<Max_Evening)
            {
                puts ("Good evening!");
                break;
            }
            if (hours>=Max_Night && hours<Max_Morning)
            {
                puts ("Good morning!");
                break;
            }
            else
            {
                puts ("Good night!");
                break;
            }
        }

    }
return 0;
}