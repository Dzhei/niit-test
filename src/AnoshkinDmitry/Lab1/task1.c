/*
 �������� ���������, ������� ����������� � ������������ ���,
���� � ���, � ����� ����������� ����������� ����� � ����, ����-
��� ������������ � ���������� ��������� (��������, ����������,
�����)
*/

// �������� ������� �.�.

#include <stdio.h>
#include <string.h>  // ��������� ������ �� ��������
#define W_Norm 110       
#define M_Norm 100
#define Norm_dev 5           // deviation from norm
#define Max_height 230
#define Max_weight 500

void clean_stdin()  // ������� ������� ������
{
     int c;
     do
     {
         c=getchar();
     }
     while (c!='\n' && c!=EOF);
}

//F10, shift+F5 - ����/����� �� �������

int main ()
{
	char gender;    //pol
    int height;     //rost
    int weight;     //ves
    int norm;
    int out;        //���������� ������ ��������� ��������
    char outstr[][20] = {"You are too fat!", "You are too thin!", "Nice job!"};

    while (1)
    {
        puts ("Hello, Enter your gender (w, m), height, weight separated by '-'");
        if(scanf ("%c-%d-%d", &gender, &height, &weight)<3 || (gender!='w' && gender !='m')
            || height<0 || height >Max_height || weight <0 || weight>Max_weight)
        {
            puts ("Input Error!");
            clean_stdin();
        }
        else
        {         
            if (gender == 'w')              //����������� ����� ��� ���������� ����
                norm=W_Norm;
            else
                norm=M_Norm;

            if (norm > height-weight+Norm_dev)
            {
                out=0;
                break;
            }
            else
                if (norm<(height-weight-Norm_dev))
                {
                     out=1;
                     break;
                }
                else 
                {
                     out=2;
                     break;
                }
            
        }
    }
    printf ("%s \n", outstr[out]);
return 0;   
}